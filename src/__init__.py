''' holds core data strcutures '''
from configparser import ConfigParser, ExtendedInterpolation
from dataclasses import dataclass, field
from string import Template
from sys import exit as sys_exit

from logging import warning, error
from typing import Union, Optional

from pandas import Timestamp, Timedelta


@dataclass
class _DocumentedData:
  ''' a bit of annotated/documented data; e.g., for user-facing data '''

  name: str
  ''' friendly name for the fact '''

  doc: str
  ''' user-facing documentation regarding the value '''

  value: float
  ''' actual numerical value of this fact '''

  unit: str
  ''' unit of value '''


@dataclass
class Fact(_DocumentedData):
  ''' a fact, directly or indirectly deduced from data sources '''

  bases: list['Fact']
  '''
  list of facts this fact is based on
  '''

  # we need to duplicate this field because fields with defaults must
  # follow fields without defaults in Python < 3.10, because
  # Python < 3.10 does not support initializing the field with
  # ``kw_only=True`` yet
  updated_at: Timestamp = field(default_factory=Timestamp.now)
  ''' timestamp this object has been created '''


@dataclass
class Rating(_DocumentedData):
  ''' just like a fact, but interpreted/opinionated '''

  bases: Union[list['Fact'], list['Rating']]
  '''
  list of facts or ratings this rating is based on
  '''

  # see comment at ``Fact.updated_at``
  updated_at: Timestamp = field(default_factory=Timestamp.now)
  ''' timestamp this object has been created '''


@dataclass
class Area:
  '''
  top-level data structure; represents an area (as we face it to
  users), including configuration and gathered facts
  '''
  code: str
  name: str
  timezone: str
  balance_areas: list[str]
  control_areas: list[str]
  rating: Optional[Rating] = None

  def __hash__(self):
    '''
    we make areas hashable, mainly for caching return values of
    procedures taking areas as argument
    '''
    return hash(self.code)


def load_areas(*filenames: str) -> list[Area]:
  '''
  main entry point; takes paths to configuration files, loads and
  processes configuration, returns resulting configuration
  '''

  # stolen from https://stackoverflow.com/a/43415907
  class SectionNameExtendedInterpolation(ExtendedInterpolation):
    '''
    ConfigParser interpolation which replaces ``${SECTION}`` with the
    current section name
    '''
    # pylint: disable=too-many-arguments
    # pylint: disable=too-many-positional-arguments
    def before_get(self, parser, section, option, value, defaults):
      ''' see class documentation '''
      value = Template(value).safe_substitute({'SECTION': section})
      return super().before_get(parser, section, option, value, defaults)

  def unpack_list(value: str) -> list[str]:
    '''
    used to unpack comma-separated lists in config files to list objects
    '''
    splits = value.split(',')
    strips = (split.strip() for split in splits)
    values = (strip for strip in strips if strip)
    return list(values)

  parser = ConfigParser(
    interpolation=SectionNameExtendedInterpolation()
  )
  parser.read(*filenames)

  areas = []

  for section in parser.sections():

    area = Area(
      code=section,
      name=parser.get(section, "name"),
      timezone=parser.get(section, "timezone"),
      balance_areas=unpack_list(parser.get(section, "balance_areas")),
      control_areas=unpack_list(parser.get(section, "control_areas")),
    )

    # check if we processed/understood all configuration options
    for option in parser.options(section):
      if not hasattr(area, option):
        warning(f'did not understand configuration option "{option}" '
                f'in section "{section}"')

    if area in areas:
      error(f'area "{section}" defined twice')
      sys_exit(1)

    areas.append(area)

  return areas
