''' module handles configuration data structures and helpers '''

from argparse import Namespace, ArgumentDefaultsHelpFormatter
from functools import cache
from logging import error
from multiprocessing import cpu_count
from pathlib import Path
import sys

from configargparse import ArgumentParser


Config = Namespace
'''
main configuration data structure; area-independent

for now, this is just an alias for ``argparse.Namespace``
'''


@cache
def get_config() -> ArgumentParser:
  '''
  acquires configuration options from CLI, environment, and
  configuration files, returns resulting configuration object
  '''
  parser = ArgumentParser(
    description=__doc__,
    default_config_files=['*.conf'],
    formatter_class=ArgumentDefaultsHelpFormatter,
  )

  parser.add('-c', '--config', is_config_file=True, type=Path,
             help='configuration file to use')
  parser.add('-a', '--areas-configs', nargs='*', default=['areas.ini'],
             type=Path, help='configuration files for areas to use')
  parser.add('-r', '--retry-count', default=6,
             help='how many times to retry failed API request')
  parser.add('-t', '--retry-wait', default=3,
             help=('how many seconds to wait between retries of failed '
                   'API request'))
  parser.add('--entsoe-api-key', env_var='ENTSOE_API_KEY',
             help='API key for transparency.entsoe.eu')
  parser.add('-p', '--parallel', type=int, default=cpu_count(),
             help=('maximum number of parallel processes; keep a '
                   'balance between required run time and API rate '
                   'limits; a run time below 15\' is usually enough '
                   'for deployments'))
  parser.add('-o', '--out-dir', default='output', env_var='OUT_DIR',
             type=Path, help='base directory for output files')
  parser.add('-u', '--base-url', default='', env_var='BASE_URL',
             help='base URL under which the output file will be served')
  parser.add('-v', '--verbose', action='store_true', default=False,
             help='turn on verbose messages')
  parser.add('-d', '--debug', action='store_true', default=False,
             help='run in debug mode, disables parallelism')

  config = parser.parse_args(namespace=Config)

  if config.entsoe_api_key is None:
    error('please provide the ENTSO-E API key')
    sys.exit(1)

  return config
