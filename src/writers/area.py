''' writes docs and values to be served as API '''

from dataclasses import asdict
from json import dump
from pathlib import Path
from typing import Union, Optional

from src import Area, Fact, Rating
from src.writers import mkdir, render, write, name_to_path


def trace_to_path(area: Area,
                  trace: Union[list['Fact'], list['Rating']],
                  suffix: Optional[str] = None) -> Path:
  ''' returns a path for the trace of ratings/facts '''
  parts = (name_to_path(p.name) for p in trace)
  path = name_to_path(area.code).joinpath(*parts)
  if suffix:
    path = path.with_suffix(suffix)
  return path


def _write_txt(area: Area, trace: Union[list['Fact'], list['Rating']]
              ) -> None:
  ''' writes data to a text file '''
  with write(trace_to_path(area, trace, '.txt')) as out_file:
    out_file.write(str(trace[-1].value))
    out_file.write('\n')


def _write_json(area: Area, trace: Union[list['Fact'], list['Rating']]
               ) -> None:
  ''' writes data to a json file '''
  in_data = trace[-1]
  out_data = asdict(in_data)
  out_data['updated_at'] = in_data.updated_at.timestamp()
  out_data['bases'] = {base.name: str(name_to_path(base.name))
                       for base in in_data.bases}
  with write(trace_to_path(area, trace, '.json')) as out_file:
    dump(out_data, out_file, indent=2)


def _write_html(area: Area, trace: Union[list['Fact'], list['Rating']]
                ) -> None:
  ''' writes data to a json file '''
  data = trace[-1]
  context = {
    'area': area,
    'data': data,
    'bases_paths': [name_to_path(b.name) for b in data.bases],
    'crumb_names': [o.name for o in trace],
    'crumb_paths': [name_to_path(o.name) for o in trace],
  }
  render(Path('layouts/data.html'), context,
         trace_to_path(area, trace, '.html'))


def _write_flags(area: Area, trace: Union[list['Fact'],
                   list['Rating']]) -> None:
  '''
  writes an empty file indicating a good or bad rating, respectively
  '''
  data = trace[-1]
  if not isinstance(data, Rating):
    return
  assert 0 <= data.value <= 1
  if data.value == 1:
    with write(trace_to_path(area, trace, '.good')) as out_file:
      out_file.write('')
  else:
    with write(trace_to_path(area, trace, '.bad')) as out_file:
      out_file.write('')


def _write_data(area: Area, trace: Union[list[Fact], list[Rating]]
               ) -> None:
  ''' writes a facts and ratings to the file system '''
  mkdir(trace_to_path(area, trace))

  for write_func in (_write_txt, _write_json, _write_flags, _write_html):
    write_func(area, trace)

  for fact in trace[-1].bases:
    _write_data(area, [*trace, fact])


def _write_area(area: Area) -> None:
  ''' writes area index page (smiley) '''
  path = name_to_path(area.code)
  assert area.rating is not None # check again to satisfies mypy
  context = {
    'area': area,
    'path': path,
  }
  render(Path('layouts/area.html'), context, path.with_suffix('.html'))


def main(area: Area) -> None:
  '''
  main entry point to write documents which are served as "API"
  '''
  _write_area(area)
  assert area.rating is not None
  _write_data(area, [area.rating])
