''' writes files which are area-independent '''

from logging import info
from pathlib import Path

from src.config import get_config
from src.writers import CONTENTS_DIR, render, copy, mkdir

def main() -> None:
  '''
  main entry point to write area-independent files to output directory
  '''
  contents_dir = Path(CONTENTS_DIR)
  context = {
    'base_url': get_config().base_url,
  }
  for in_path in contents_dir.rglob('*'):
    out_path = in_path.relative_to(contents_dir)
    if in_path.is_dir():
      mkdir(out_path)
    elif in_path.suffix == '.jinja':
      info(f'rendering: {in_path}')
      render(in_path, context, out_path.with_suffix(''))
    else:
      info(f'copying: {in_path}')
      copy(in_path, out_path)
