''' writes files which are area-independent '''

from logging import info
from pathlib import Path
from typing import Iterable

from requests import Session
from requests.adapters import HTTPAdapter

from src import Area
from src.writers import render, write, name_to_path


FLAG_CODE_MAP = {
  'uk': 'gb',
}


def svg_flag(area: Area) -> str:
  ''' returns the flag of the area as SVG in a string '''

  code: str = area.code.lower()
  code = FLAG_CODE_MAP.get(code, code)

  try:
    # making mypy happy is cumbersome in this case and does not help
    # readability, thus, ignore type error:
    session = svg_flag.session # type: ignore
  except AttributeError:
    session = Session()
    session.mount('https://', HTTPAdapter(max_retries=3))
    svg_flag.session = session # type: ignore

  for code in (code, 'xx'):
    response = session.get(
      f'https://flagicons.lipis.dev/flags/4x3/{code}.svg', timeout=10
    )
    if 'image/svg' not in response.headers['content-type']:
      continue
    try:
      return response.text
    except AttributeError:
      continue

  assert False, 'the "unknown"/"xx" flag should have been found'


def main(areas: Iterable[Area]) -> None:
  '''
  main entry point to write files depending on all (processed) areas to
  output directory
  '''
  info('rendering areas overview')
  areas = [area for area in areas if area.rating is not None]
  areas = sorted(areas, key=lambda a: a.name)
  paths = [name_to_path(a.code) for a in areas]

  for area, path in zip(areas, paths):
    svg_path = path.with_suffix('.svg')
    if svg_path.exists():
      continue
    with write(svg_path) as out_file:
      out_file.write(svg_flag(area))

  context = {
    'areas': areas,
    'paths': paths,
  }
  render(Path('layouts/areas.html'), context, Path('areas.html'))
