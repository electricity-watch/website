'''
module holds all classes that write Facts/Ratings to the file system
'''

from functools import cache
from importlib import import_module
from io import TextIOWrapper
from os.path import relpath
from pathlib import Path
from pkgutil import walk_packages
from re import compile as re_compile
from shutil import copy as shutil_copy
from typing import Iterable, Union
from types import ModuleType

import jinja2

from src.config import get_config


_LAYOUTS_DIRECTORY = 'layouts'
CONTENTS_DIR = 'contents'


_TO_PATH_NAME_REPLACEMENTS = (
  (re_compile(r'\s'), '-'),
  (re_compile(r'[^\w-]'), '_'),
)


def name_to_path(name: str) -> Path:
  '''
  returns a (path) name with characters which are unfavorable in path
  names removed
  '''
  name = name.lower()
  for pattern, new in _TO_PATH_NAME_REPLACEMENTS:
    name = pattern.sub(new, name)
  return Path(name)


@cache
def get_jinja_environment() -> jinja2.Environment:
  ''' returns a Jinja2 environment with a file system loader '''
  return jinja2.Environment(
    loader=jinja2.FileSystemLoader(searchpath=['.', _LAYOUTS_DIRECTORY])
  )


def output_path(path: Path) -> Path:
  '''
  returns the specified path relative to the configured output
  directory; creates top-most output directory
  '''
  out_dir = get_config().out_dir
  if not out_dir.exists():
    out_dir.mkdir(parents=True)
  return out_dir/path


def mkdir(path: Path) -> None:
  ''' ensures the specified path exists as directory '''
  output_path(path).mkdir(parents=True, exist_ok=True)


def write(path: Path) -> TextIOWrapper:
  ''' returns an opened file for writing '''
  return open(output_path(path), 'w', encoding='utf-8')


def copy(in_path: Path, path: Path) -> None:
  ''' ensures the specified path exists as directory '''
  shutil_copy(in_path, output_path(path))


def render(template: Path, context: dict, path: Path) -> None:
  ''' renders a template file with a context into an output file '''
  context['base_url'] = get_config().base_url
  with open(output_path(path), 'w', encoding='utf-8') as out_file:
    out_file.write(
      get_jinja_environment(
      ).get_template(
        str(template)
      ).render(
        **context
      )
    )
