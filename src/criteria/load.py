'''
this criterion indicates whether there is an above-average (bad) or
below-average (good) load in the grid of an area
'''

from functools import cache
from logging import info
from typing import Optional

from pandas import Timestamp, Timedelta

from src import criteria, Fact, Rating, Area


@cache
def get_average(area: Area) -> Fact:
  ''' returns the average load '''

  entsoe = criteria.get_entsoe_client()
  end = Timestamp(Timestamp.now().date(), tz=area.timezone)
  delta = Timedelta(days=365)
  start = end - delta

  try:
    average = entsoe.query_load(
      area.code, start=start, end=end
    )['Actual Load'].mean()
  except criteria.NoMatchingDataError as exception:
    raise criteria.MissingDataException from exception
  return Fact(
    name='load average',
    doc=('Average load in the grid, calculated from data of the '
         f'past {delta.days} days. Seasonal effects are not '
         'considered, because the capacity in the grid is determined '
         'by high loads. For example, the grid will not be sized down '
         'during summer, although the load average is lower than in '
         'winter. It is therefore not unsustainable to use the free '
         'below-average capacity of the grid.'),
    value=average,
    unit='MW',
    bases=criteria.get_entsoe_base_facts()
  )


@cache
def get_forecast(area: Area) -> Fact:
  '''
  returns the for now forecasted load
  '''
  entsoe = criteria.get_entsoe_client()
  forecast = criteria.query_latest(
    area, entsoe.query_load_forecast, area.code
  )['Forecasted Load']

  return Fact(
    name='load forecast',
    doc='Forecasted load for the current point in time.',
    value=forecast,
    unit='MW',
    bases=criteria.get_entsoe_base_facts()
  )


@cache
def get_latest_with_forecast(area: Area) -> tuple[Fact, Fact]:
  '''
  returns the forecast correction, based on the latest reported actual
  load value and the corresponding forecasted load
  '''
  entsoe = criteria.get_entsoe_client()
  latest_and_forecast = criteria.query_latest(
    area, entsoe.query_load_and_forecast, area.code
  )

  actual = latest_and_forecast['Actual Load']
  if actual <= 0:
    raise criteria.ImplausibleDataException(
      f'actual load for {area.name} seems to be implausible ({actual})'
    )

  forecast = latest_and_forecast['Forecasted Load']
  if forecast <= 0:
    raise criteria.ImplausibleDataException(
      f'forecasted load for {area.name} seems to be implausible '
      f'({forecast})'
    )

  return Fact(
    name='latest reported load',
    doc='Latest reported actual load in the area.',
    value=actual,
    unit='MW',
    bases=criteria.get_entsoe_base_facts()
  ), Fact(
    name='forecasted load for latest reported load',
    doc='Load that has been forecasted for the latest actual load.',
    value=forecast,
    unit='MW',
    bases=criteria.get_entsoe_base_facts()
  )


def main(area: Area) -> Optional[Rating]:
  '''
  main coordination to gather facts about current load in the grid
  '''
  info(f'determining load rating for {area.name}')

  try:
    load_forecast = get_forecast(area)
  except criteria.MissingDataException:
    info(f'no load forecast data for {area.name}')
    return None

  forecast = Fact(
    name='estimated load',
    doc=('Load estimated for the current point in time.'),
    value=load_forecast.value,
    unit='MW',
    bases=[load_forecast]
  )

  try:
    latest_actual, forecast_for_latest_actual = \
      get_latest_with_forecast(area)
  except criteria.DataException:
    info(f'no load forecast correction data for {area.name}')
    correction = None
  else:
    correction = Fact(
      name='load forecast correction',
      doc=('Correction which has to be added to the load forecast.'),
      value=latest_actual.value-forecast_for_latest_actual.value,
      unit='MW',
      bases=[latest_actual, forecast_for_latest_actual]
    )

  if correction:
    forecast.value += correction.value
    forecast.bases.append(correction)

  try:
    average = get_average(area)
  except criteria.MissingDataException:
    info(f'no load average data for {area.name}')
    return None

  relative = Fact(
    name='relative load',
    doc='Current estimated load in relation to average load.',
    value=100*forecast.value/average.value,
    unit='%',
    bases=[forecast, average]
  )

  return Rating(
    name='grid load rating',
    doc=__doc__.strip().replace('\n', ' '),
    value=min(1, 100/relative.value),
    unit='rating',
    bases=[relative]
  )
