'''
module holds all criteria influencing the decision (whether it is a good
time to consume electricity or not); criteria are modules which acquire
facts and to finally judge the respective "rating" (a value between
0=bad and 1=good)
'''

from importlib import import_module
from logging import debug
from pkgutil import walk_packages
from types import ModuleType
from typing import Callable

from entsoe import EntsoePandasClient
from entsoe.exceptions import NoMatchingDataError
from pandas import DataFrame, Series, Timestamp, Timedelta, concat

from src import Area, Fact
from src.config import get_config


class DataException(BaseException):
  '''
  common base exception for all exceptions where there are problems with
  the required data
  '''


class MissingDataException(DataException):
  '''
  general exception to indicate missing data, e.g., APIs do not return
  what is expected or required
  '''


class ImplausibleDataException(DataException):
  '''
  general exception to indicate implausible data, e.g., APIs return
  data which is likely wrong
  '''


def get_today(area: Area) -> Timestamp:
  ''' returns timestamp of today('s date) '''
  return Timestamp(Timestamp.now().date(), tz=area.timezone)


def get_criteria() -> dict[str, ModuleType]:
  ''' returns all criteria names and modules '''
  return {
    package.name: import_module(f'{__name__}.{package.name}')
    for package in walk_packages(__path__)
  }


def get_entsoe_client() -> EntsoePandasClient:
  '''
  returns an Entsoe API client, configured according to ``config``
  '''
  config = get_config()
  return EntsoePandasClient(
    api_key=config.entsoe_api_key,
    retry_count=config.retry_count,
    retry_delay=config.retry_wait,
  )


def get_entsoe_base_facts() -> list[Fact]:
  ''' returns a fact the represents the entso-e API '''
  return [
      Fact(
      name='ENTSO-E',
      doc=('Data acquired from European Network of Transmission System '
           'Operators for Electricity (ENTSO-E) API.'),
      value=float('nan'),
      unit='n/a',
      bases=[]
    )
  ]


def latest_from_data(data: DataFrame, max_null_rate=.5) -> Series:
  '''
  returns latest data series from data frame, where less than
  :para:`max_null_rate` values are not null/NaN
  '''
  # drop completely empty rows
  data = data.dropna(how='all')

  # accessing rows reverse (instead of sorting reverse in beforehand)
  for row_num in range(len(data.index)):
    row = data.iloc[-1+row_num]
    if row.isnull().sum() / row.size > max_null_rate:
      debug('too much NaN')
      continue
    return row

  return None


def query_latest(area: Area, func: Callable, *args,
                 max_null_rate=.5, **kwargs) -> Series:
  '''
  queries, selects, and returns latest available data using the
  function, arguments given
  '''
  assert 'start' not in kwargs and len(args) < 2, 'conflicting argument'
  assert 'end' not in kwargs, 'conflicting argument'

  debug_str = f'func={func.__name__}, args={args}, kwargs={kwargs}'

  end = Timestamp.now(tz=area.timezone)
  start = end - Timedelta(hours=3)
  try:
    data = func(*args, **kwargs, start=start, end=end)
  except NoMatchingDataError:
    debug(f'{debug_str}: no matching data')
  else:
    row = latest_from_data(data, max_null_rate)
    if row is not None:
      return row

  raise MissingDataException(
    f'{debug_str}: could not get (enough) latest data from; '
    'queried back until {start}'
  )


def query_history_for_seasonal_data(area: Area, days_count: int,
                                    func: Callable, *args, **kwargs
                                   ) -> DataFrame:
  '''
  queries and returns historic data using the function, arguments
  given; especially for data which exposes seasonal effects by combining
  data from before today and after the same data of today last year
  '''
  assert 'start' not in kwargs and len(args) < 2, 'conflicting argument'
  assert 'end' not in kwargs, 'conflicting argument'

  half_delta = Timedelta(days=days_count/2)
  today = get_today(area)

  intervals = (
    (
      today - half_delta,
      today
    ),
    (
      today - Timedelta(days=365),
      today - Timedelta(days=365) + half_delta
    ),
  )

  results = []
  for start, end in intervals:
    try:
      result = func(*args, start=start, end=end, **kwargs)
    except NoMatchingDataError as exception:
      raise MissingDataException(
        f'could not get historic data from func={func.__name__}, '
        f'args={args}, kwargs={kwargs} between {start} and {end}'
      ) from exception

    results.append(result)

  return concat(results)


def doc_history_for_seasonal_data(days_count: int) -> str:
  '''
  returns a bit of documentation text for data which has been acquired
  using :func:`query_history_for_seasonal_data`
  '''
  half_days_count = int(days_count / 2)
  return (
    'One half of the historic data is taken from a time span before '
    f'today (i.e., between -{half_days_count} days and today), '
    'the other half is taken from a time span after today\'s date last '
    f'year (i.e., between -365 and -{365 - half_days_count} days). '
    'These time spans account for historic data with seasonal effects.'
  )
