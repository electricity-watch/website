'''
This criterion indicates whether or not the available energy comes from
(considered) good generation sources (e.g., renewable, geothermal) or
not (e.g., fossils, nuclear).
'''

from logging import warning, info
from typing import Optional

from entsoe.exceptions import NoMatchingDataError
from pandas import Series, concat

from src import criteria, Fact, Rating, Area


GOOD_SOURCES = {
  'Geothermal',
  'Hydro Run-of-river and poundage',
  'Other renewable',
  'Solar',
  'Wind Offshore',
  'Wind Onshore',
}


BAD_SOURCES = {
  'Biomass',
  'Fossil Brown coal/Lignite',
  'Fossil Coal-derived gas',
  'Fossil Gas',
  'Fossil Hard coal',
  'Fossil Oil',
  'Fossil Oil shale',
  'Fossil Peat',
  'Hydro Pumped Storage',
  'Hydro Water Reservoir',
  'Marine', # what is this?
  'Nuclear',
  'Other',
  'Waste',
}


def good_and_total(generation: Series) -> tuple[Fact, Fact]:
  '''
  calculates good and total power generation from a single
  generation-per-source result
  '''

  # check if we know/understand all the given generation sources
  for source in set(generation.index) - GOOD_SOURCES - BAD_SOURCES:
    warning(f'unknonw generation source: {source}')

  # request to sum only those sources which are available
  available_good_sources = [s for s in GOOD_SOURCES
                            if s in generation.keys()]

  good = generation[available_good_sources].sum()
  if good < 0:
    raise criteria.ImplausibleDataException(
      f'sum of energy generation form good sources seems to be '
      f'implausible ({good})'
    )

  total = generation.sum()
  if total <= 0:
    raise criteria.ImplausibleDataException(
      f'total energy generation seems to be implausible ({total})'
    )

  assert total > 0

  return Fact(
    name='good energy generated',
    doc=f'Amount of energy generated from '
        f'{", ".join(available_good_sources)}.',
    value=good,
    unit='MW',
    bases=criteria.get_entsoe_base_facts()
  ), Fact(
    name='total energy generated',
    doc='Total amount of energy generated.',
    value=total,
    unit='MW',
    bases=criteria.get_entsoe_base_facts()
  )


def main(area: Area) -> Optional[Rating]:
  ''' main entry point to get generation result '''
  info(f'determining sources rating for {area.name}')

  if not area.control_areas:
    return None

  def _query_generation(*args, **kwargs):
    '''
    just like ``query_generation`` of an ``EntsoePandasClient``, but
    drops columns containing consumption (instead of generation) values
    '''
    entsoe = criteria.get_entsoe_client()

    results = []
    for control_area in area.control_areas:

      try:
        result = entsoe.query_generation(control_area, *args, **kwargs)
      except NoMatchingDataError as exception:
        raise criteria.MissingDataException from exception

      if result.columns.nlevels > 1:

        result = result.drop('Actual Consumption', axis=1, level=1)
        assert {*result.columns.get_level_values(1)} == {'Actual Aggregated'}
        result = result.droplevel(1, axis=1)
        assert result.columns.nlevels == 1

      # drop all-zero rows (some areas return such if data is missing)
      result = result.loc[~(result==0).all(axis=1)]

      results.append(result)

    assert len(results) > 0
    return concat(results)

  try:
    latest = criteria.query_latest(area, _query_generation)
  except criteria.MissingDataException:
    info(f'no actual generation data for {area.name}')
    return None

  try:
    latest_good, latest_total = good_and_total(latest)
  except criteria.ImplausibleDataException:
    info(f'implausbile generation data for {area.name}')
    return None

  latest_share = Fact(
    name='latest good energy share',
    doc=('Current share of energy generated from sources considered '
         'good.'),
    value=100*latest_good.value/latest_total.value,
    unit='%',
    bases=[latest_good, latest_total]
  )

  days_count = 50

  try:
    data = criteria.query_history_for_seasonal_data(area, days_count,
                                                    _query_generation)
  except criteria.MissingDataException:
    info(f'no average generation data for {area.name}')
    return None

  average = data.mean()
  average_good, average_total = good_and_total(average)
  average_share = Fact(
    name='average good energy share',
  doc=(f'Share of electricity generated from good sources, '
       f'averaged over a time span of {days_count} days. '
       + criteria.doc_history_for_seasonal_data(days_count)),
    value=100*average_good.value/average_total.value,
    unit='%',
    bases=[average_good, average_total]
  )

  return Rating(
    name='energy source rating',
    doc=__doc__.strip().replace('\n', ' '),
    value=min(1, latest_share.value / average_share.value),
    unit='rating',
    bases=[latest_share, average_share]
  )
