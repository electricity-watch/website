'''
This criterion indicates whether there is an overproduction (good) or
underproduction (bad) of energy in an area.
'''

from functools import cache
from logging import info
from typing import Optional

from entsoe.exceptions import NoMatchingDataError
from numpy import clip
from pandas import DataFrame, concat

from src import criteria, Fact, Rating, Area


@cache
def get_historic_data(area: Area) -> DataFrame:
  ''' returns historic imbalance volumes '''
  entsoe = criteria.get_entsoe_client()
  days_count = 200

  results = []
  for balance_area in area.balance_areas:
    try:
      result = criteria.query_history_for_seasonal_data(
        area, days_count, entsoe.query_imbalance_volumes, balance_area
      )
    except NoMatchingDataError as exception:
      raise criteria.MissingDataException from exception
    results.append(result)

  assert len(results) > 0
  return concat(results)


@cache
def get_standard_deviation(area: Area) -> Fact:
  ''' returns standard deviation of historic imbalance volumes '''
  data = get_historic_data(area)
  days_count = len(data.groupby(data.index.day)) - 1
  return Fact(
    name='imbalance volumes standard deviation',
    doc=('Standard deviation of imbalance volumes within '
         f'{days_count} days. '
         + criteria.doc_history_for_seasonal_data(days_count)),
    value=data.std().iloc[0],
    unit='MWh',
    bases=criteria.get_entsoe_base_facts()
  )


@cache
def get_average(area: Area) -> Fact:
  ''' returns average of historic imbalance volumes '''
  data = get_historic_data(area)
  days_count = len(data.groupby(data.index.day)) - 1
  return Fact(
    name='imbalance volumes average',
    doc=(f'Average imbalance volumes within {days_count} '
         'days. ' + criteria.doc_history_for_seasonal_data(days_count)),
    value=data.mean().iloc[0],
    unit='MWh',
    bases=criteria.get_entsoe_base_facts()
  )


@cache
def get_latest(area: Area) -> Fact:
  ''' returns latest reported imbalance volume '''
  entsoe = criteria.get_entsoe_client()
  latest = [
    criteria.query_latest(
      area, entsoe.query_imbalance_volumes, balance_area
    )
    for balance_area in area.balance_areas
  ]
  return Fact(
    name='imbalance volume',
    value=sum(l['Imbalance Volume'] for l in latest),
    unit='MWh',
    doc=('Amount of excess energy generated or consumed. Positive '
         'values indicate excess generation (energy surplus), negative '
         'values indicate excess consumption (energy deficit).'),
    bases=criteria.get_entsoe_base_facts()
  )


def main(area: Area) -> Optional[Rating]:
  ''' main entry point to get imbalance rating '''
  info(f'determining imbalance rating for {area.name}')
  if not area.balance_areas:
    return None
  try:
    imbalance = get_latest(area)
  except criteria.MissingDataException:
    info(f'no imbalance volumes for {area.name}')
    return None

  rating_doc = (
    'Rates according to the imbalance volumes in the area. In order to '
    'maintain grid stability, the amount of energy generated must be '
    'in balance with the amount of energy consumed. Operators must '
    'counter imbalances with, e.g., increasing or decreasing energy '
    'production. Hence, generation reserves must be kept available. '
    'Reducing the need for keeping available and activating generation '
    'reserves would increase sustainability. Therefore, imbalance '
    'volumes should be above average (i.e., less average deficit) and '
    'below the standard deviation (i.e., less average reserves).'
  )

  try:
    average = get_average(area)
    standard_deviation = get_standard_deviation(area)
  except criteria.MissingDataException:
    info(f'No historic imbalance volumes for {area.name} (for relative '
         'imbalance rating), falling back to rating by sign '
         '(overproduction → 1, else → 0)')
    rating = Rating(
      name='imbalance rating',
      doc=(rating_doc + ' Rated by sign of imbalance volumes '
           ' (1 for a overproduction, else rated 0).'),
      value=1 if imbalance.value < 0 else 0,
      unit='rating',
      bases=[imbalance]
    )
  else:
    relative = Fact(
      name='relative imbalance',
      doc=('Imbalance volume relative to its average and '
           'standard deviation (mathematically: '
           '(current - average) / standard deviation).'),
      value=(100 *
        (imbalance.value - average.value) / standard_deviation.value
      ),
      unit='%',
      bases=[imbalance, average, standard_deviation]
    )
    rating = Rating(
      name='energy imbalance rating',
      doc=rating_doc,
      value=clip(1+relative.value/100, 0, 1),
      unit='rating',
      bases=[relative]
    )

  return rating
