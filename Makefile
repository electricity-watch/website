MAKEFLAGS += -r

MODULES = electricity-watch src

OUT_DIR=output
OUT_DIR_TEMPLATE := $(OUT_DIR).XXXX
OUT_DIR_NEW := $(shell mktemp -ud $(OUT_DIR_TEMPLATE))
OUT_DIR_OLD := $(shell mktemp -ud $(OUT_DIR_TEMPLATE))
EW_OPTS=-v -o $(OUT_DIR)

DEVSERVER_PORT ?= 8585

PIP_OPTS ?= --upgrade --upgrade-strategy=eager
ifeq ($(VIRTUAL_ENV),)
  PIP_OPTS += --user
endif

# lazy ftw
help:
	@awk '{$$1=$$1};1' README.rst \
	| grep -B1 -A1 --no-group-separator '^make '

_setup:
	pip install $(PIP_OPTS) -r $? $(PIP_REQ_FILE)

setup: PIP_REQ_FILE=requirements.txt
setup: _setup

setup-run: setup

setup-dev: PIP_REQ_FILE=requirements-dev.txt
setup-dev: _setup

run:
	./electricity-watch $(EW_OPTS)

run-test:
	./electricity-watch $(EW_OPTS) -da test-areas.ini

production:
	@# Since this target is used for the deployment, we build to a temporary
	@# directory and move that output to the actual output directory upon
	@# success. That way, we have no half-built output in case of failures.
	$(MAKE) OUT_DIR=$(OUT_DIR_NEW) run || (rm -rf $(OUT_DIR_NEW); exit)
	chmod -R g+rX $(OUT_DIR_NEW)
	[ -d $(OUT_DIR) ] && mv $(OUT_DIR) $(OUT_DIR_OLD) || true
	mv $(OUT_DIR_NEW) $(OUT_DIR)
	rm -rf $(OUT_DIR_OLD) &

	# compress non-binary files of at least 32 bytes
	# (gzip preserves file permissions)
	find $(OUT_DIR) -type f -size +32c -print0 \
	| xargs -0 file -iN | grep -Iv ' charset=binary' \
	| rev | cut -d: -f2- | rev \
	| xargs -d '\n' -P $$(nproc) -n 10 gzip --keep --best --force

pylint: $(MODULES)
	pylint --rcfile=.pylintrc $?

mypy: $(MODULES)
	mypy $?

qa: mypy pylint

devserver:
	cd $(OUT_DIR) && python3 -m http.server --bind 127.0.0.1 $(DEVSERVER_PORT)

clean:
	rm -rf $(shell echo $(OUT_DIR_TEMPLATE) | sed 's/X/?/g')

distclean: clean
	rm -rf \
		$(OUT_DIR) \
		.mypy_cache
	find -name __pycache__ -or -name '*.pyc' -delete
