# file where compression makes sense come pre-compressed, so forbid nginx
# to compress files on its own but server <url>.gz files, if present
gzip off;
gzip_static on;

# no caching for html files
location ~* \.html?$ {
  sendfile off;
  # force clients to check for new content
  add_header Cache-Control "no-cache";
}

# redirect to area …
# (put all locations that need to be redirected to an area into a single
# block, so the ``set_by_lua`` directive does not need be computed for
# every request or duplicated)
location ~ ^/$|/auto((\.|/)(.*))?$ {

  # get the country code detected by GeoIP module in lower case:
  set_by_lua $country_code_lower "return ngx.arg[1]:lower()" $country_code;

  # … unconditionally for non-root URLs
  if ( $request_uri != / ) {
    return 302 /$country_code_lower$1;
  }

  # … if it exists for the root URL
  if ( -f $realpath_root/$country_code_lower.html ) {
    return 302 /$country_code_lower;
  }

}

location / {

  index index.html;
  try_files $uri $uri.html $uri/ =404;

  fancyindex on;
  fancyindex_header /nginx-fancy-index-header.html;
  fancyindex_footer /nginx-fancy-index-footer.html;

  error_page 404 /404.html;

  # behind reverse proxies with SSL: prevent redirects to ``http://…``
  absolute_redirect off;

  # no URL parameters
  if ( $request_uri ~ ^(.*)\?.*$ ) {
    return 301 $1;
  }

  # no trailing ``htm[l]``
  if ( $request_uri ~ ^(.*)\.html?$ ) {
    return 301 $1;
  }

  # no trialing ``index``
  if ( $request_uri ~ ^(.*)index$ ) {
    return 301 $1;
  }

}
