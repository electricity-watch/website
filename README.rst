|ew|
====

.. image:: https://gitlab.com/lpirl/electricity-watch/badges/main/pipeline.svg
  :target: https://gitlab.com/lpirl/electricity-watch/pipelines
  :align: right

This repository contains the source code and corresponding documentation
regarding the website of |ew|. For a general introduction of the
project, please see `electricity.watch/about
<https://electricity.watch/about>`__.


general overview
================

The |ew| website is powered by a Python script which regularly creates
the files to serve. The script uses data from APIs to rate whether it
is a good time to consume electricity in a specific area or not. The
output data is written to files of various formats (HTML, JSON, etc.).
Besides the aforementioned dynamically generated documents, there are
also static pages which are rendered to the output directory, and
static files which are copied to the output directory.


getting started
===============

If you want to **run** |ew| locally, this is how to get started:

.. code:: shell

  # have at least Python 3 and ``pip`` installed:
  apt install python3-pip

  # clone this repository:
  git clone git@gitlab.com:electricity-watch/website.git

  cd website

  # optional but recommended, create and activate a virtual environment:
  python3 -m venv venv
  . venv/bin/activate

  # install Python dependencies
  pip install -r requirements-run.txt

  # run for all areas
  ./electricity-watch -v


If you have make installed, the following shortcuts are available:

.. code:: shell

  # install Python dependencies to run the code
  make setup

  # run the code
  make run


For **development** the following commands might be helpful:

.. code:: shell

  # the Makefile provides shortcuts, to use those, install make:
  apt install make

  # install development dependencies
  make setup-dev

  # run for just a few areas to see if things work:
  make run-test

  # run quality assurance tests:
  make qa

  # run just pylint of quality assurance tests:
  make pylint

  # run just mypy of quality assurance tests:
  make mypy

For **deployment** there is a special shortcut which does some extra
tasks:

.. code:: shell

  # run for all areas plus some extra safety measures and tasks
  make production


URLs
====

All relative to the base URL:

:``/<area>…``:
  All endpoints regarding an area.
  ``<area>`` mostly refers to country codes.

:``/<area>…``:
  Returns data as HTML. The Web server is responsible for transparently
  (i.e., no redirect) adding the ``.html`` file extension.

:``/<area>….txt``:
  Returns data in plain text (without unit).

:``/<area>….json``:
  Returns data as JSON.

:``/<area>….not-good``:
  If the endpoint is a rating and not rated good (a value below 1),
  then this endpoint is present (HTTP 200 OK); if the rating is good (a
  value of 1), then this endpoint is not present (HTTP 404 Not Found).

  If an area cannot be rated (e.g., due to missing data), this endpoint
  will not exist as well. This endpoint can thus be used to wait for
  good electricity (e.g., in a script), because it won't block
  infinitely for areas where no rating is possible::

    while wget -qO/dev/null https://electricity.watch/auto.not-good; do
      sleep 15m;
    done
    ./expensive-script.sh

  Note that this would not work with ``/<area>….good``, which would
  block indefinitely for unknown areas and areas where a rating is not
  passible.

:``/areas``:
  Lists all available areas.


deployment notes
================

This section is largely #todo.


hosting
-------

* try to serve ``<url>.html`` before ``<url>/``
* for ``<url>/``, serve index (or not) according to your preference


Location-aware redirects
........................

:``/`` → ``/<area>``:
  The endpoint where visitors most likely land on. Therefore, it should
  |temp| to the area the visitor connects from.

:``/auto…`` → ``/<area>…``:
  For location-independent use of the "API".


URL normalization
.................

:``<upper-case characters>`` → ``<lower-case characters>``:
  URLs containing upper-case letters must |perm| to the same URL
  converted to lower-case characters.

:``….html`` → ``…``:
  URLs with a trailing ``.html`` file extension, must |perm| to the same
  URL with the file extension removed.

:``…/index`` → ``…/``:
  URLs with a trailing ``index`` file extension, must |perm| to the same
  URL with ``index`` removed.


setup
-----

For nginx there is the an `example configuration file
<doc/nginx-snippet.conf>`__ which can be used like so:

.. code::

  server {

    …

    root …/electricity-watch/output;

    include …/electricity-watch/doc/nginx-snippet.conf;
  }

If there is traffic from private networks, visitors' locations cannot be
detected based on their IP addresses. In this case, set the country code
manually, e.g.:

.. code::

  server {

    …

    if ($internal) {
      set $country_code DE;
    }

    include …/nginx-snippet.conf;
  }

  geo $internal {
    default 0;
    10.0.0.0/8 1;
    172.16.0.0/12 1;
    192.168.0.0/16 1;
    fd00::/8 1;
  }

.. ---------------------------------------------------------------------

.. shortcuts:

.. |ew| replace:: `electricity.watch <https://electricity.watch>`__

.. |perm| replace:: permanently redirect (HTTP 301)
.. |temp| replace:: temporarily redirect (HTTP 302)
